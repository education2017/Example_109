package hr.ferit.bruno.example_109;

import android.app.Activity;
import android.media.AudioManager;
import android.media.Image;
import android.media.MediaPlayer;
import android.media.SoundPool;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;

import java.io.IOException;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnCheckedChanged;
import butterknife.OnClick;

public class MainActivity extends Activity implements MediaPlayer.OnPreparedListener {

    // https://github.com/google/ExoPlayer
    // https://www.androidhive.info/2012/03/android-building-audio-player-tutorial/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        this.loadSounds();
        this.prepareMediaPlayer();
    }

    //region SoundPool

    @BindView(R.id.ibRifle) ImageButton ibRifle;
    @BindView(R.id.ibSniper) ImageButton ibSniper;
    @BindView(R.id.ibGun) ImageButton ibGun;

    SoundPool mSoundPool;
    boolean mIsLoaded = false;
    private HashMap<Integer, Integer> mSoundMap = new HashMap<>();

    private void loadSounds() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            this.mSoundPool = new SoundPool.Builder().setMaxStreams(10).build();
        }else{
            this.mSoundPool = new SoundPool(10, AudioManager.STREAM_MUSIC,0);
        }

        this.mSoundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                Log.d("Test",String.valueOf(sampleId));
                mIsLoaded = true;
            }
        });
        this.mSoundMap.put(R.raw.rifle, this.mSoundPool.load(this, R.raw.rifle,1));
        this.mSoundMap.put(R.raw.gun, this.mSoundPool.load(this, R.raw.gun,1));
        this.mSoundMap.put(R.raw.sniper,this.mSoundPool.load(this, R.raw.sniper,1));
    }

    @OnClick({R.id.ibGun, R.id.ibSniper, R.id.ibRifle})
    public void shootGunClicked(ImageButton button){
        switch (button.getId()){
            case R.id.ibGun: playSound(R.raw.gun); break;
            case R.id.ibSniper: playSound(R.raw.sniper); break;
            case R.id.ibRifle: playSound(R.raw.rifle); break;
        }
    }

    private void playSound(int resourceId) {
        int soundID = this.mSoundMap.get(resourceId);
        this.mSoundPool.play(soundID, 1,1,1,0,1F);
    }

    //endregion

    //region MediaPlayer

    @BindView(R.id.ibPlayMusic) ImageButton ibPlayMusic;

    MediaPlayer mMediaPlayer;
    boolean mPlayerReady = false;

    private void prepareMediaPlayer() {
        Uri songUri = Uri.parse("android.resource://"
            + this.getPackageName() + "/" + R.raw.song);
        Log.d("TAG", songUri.toString());

        try {
            this.mMediaPlayer = new MediaPlayer();
            this.mMediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
            this.mMediaPlayer.setDataSource(this,songUri);
            this.mMediaPlayer.prepareAsync();
            this.mMediaPlayer.setOnPreparedListener(this);
        } catch (IOException e) {
            Log.e("Error", e.getMessage());
        }
    }

    @OnClick(R.id.ibPlayMusic)
    public void playMusic(){
        if(this.mPlayerReady == false){
            Toast.makeText(this,"Player not ready", Toast.LENGTH_SHORT).show();
            return;
        }

        if(this.mMediaPlayer.isPlaying() == false){
            this.mMediaPlayer.start();
            this.ibPlayMusic.setImageResource(android.R.drawable.ic_media_pause);
        }else{
            this.mMediaPlayer.pause();
            this.ibPlayMusic.setImageResource(android.R.drawable.ic_media_play);
        }
    }

    @Override
    public void onPrepared(MediaPlayer mp) {
        Toast.makeText(this,"Player ready", Toast.LENGTH_SHORT).show();
        this.mPlayerReady = true;
    }
    //endregion





}
